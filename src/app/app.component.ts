import { Component } from '@angular/core';
import { NodeCaller } from './Caller/nodeCaller';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'sold-dobot';
}


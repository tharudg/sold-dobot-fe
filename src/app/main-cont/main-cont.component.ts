import { Component, OnInit } from '@angular/core';
import { NodeCaller } from '../Caller/nodeCaller';
import { observable, Observable } from 'rxjs';
import { MainStoreService } from '../store/main-store.service';
import { CallerPath } from '../Caller/caller.path';

@Component({
  selector: 'app-main-cont',
  templateUrl: './main-cont.component.html',
  styleUrls: ['./main-cont.component.css']
})
export class MainContComponent implements OnInit {
 
  headElements = ['ID', 'X position', 'Y position'];
  scannedImage = '';
  selectedPoints:Point[] = [];
  isUploading:boolean = false;
  isSoldering:boolean = false;
  socket: any;
  y0 = 114;
  
  
  currentPosition: Point = {
    x:0,
    y:0
  }


  constructor(
    public call:NodeCaller,
    public mainStore:MainStoreService,
    public pathCaller:CallerPath
  ) {}


  ngOnInit() {
    
  }


  getImage(){
    this.isSolderingFunc(true);
    this.selectedPoints=[];
    this.scannedImage = `https://www.aat-corp.com/wp-content/uploads/2019/06/pcb.jpg`;
    this.call.getImage().then((data)=>{
      let res = data.toString().split("/", 10);
      // this.scannedImage = `http://${this.pathCaller.ip}/assets/scanned/${res['8']}`;
      
      console.log(res);
      
      setTimeout(()=>{
        this.isUploading = false
      },1000)
      // this.ngOnInit();
    })
    .catch((e)=>{alert(e)});
    this.isSolderingFunc(false);
  }


  onMouse(e){
    this.currentPosition = {
      x: e.offsetX+10,
      y: e.offsetY-5
    } 
  }

  onClikPoint(){
    if(this.scannedImage !=''){this.selectedPoints.push(this.currentPosition);}
  }


  onRemove(index: number){
    this.selectedPoints.splice(index, 1);
  }


 async solderPoints(){
    
    console.log(this.selectedPoints);
    let isd =await this.isSolderingFunc(true);
    let selectedPoint:SoldPoint ={angleStep :0, length:0};

    for (let i = 0; i < this.selectedPoints.length; i++) {
     
      selectedPoint.angleStep =await this.calculateAngle(this.selectedPoints[i].x,this.selectedPoints[i].y);
      selectedPoint.length = await this.calculateLength(this.selectedPoints[i].x,this.selectedPoints[i].y);
      
      // this.call.setPoints(selectedPoint).then((data)=>{console.log(data)}).catch((e)=>{alert(e)}).then(()=>{})
      
      console.log("for i : "+i);
      this.sleepDelay(5000);

      
    }
    this.isSolderingFunc(false);
  }


  abort(){
    this.mainStore.isSoldering = false;
  }

  
  async calculateLength(x,y){
   
    let y1=500-y+this.y0;
    let x1:number = 0;
    
    if(x<400){
      x1 = 400-x;
    }
    else{
      x1 = x-400;
    }

    length = Math.sqrt((y1)^2+ (x1)^2);
    console.log("lenght :  ",length)
    length=(length-10)*15/11;
    console.log("Calculated lenght :  ",length);
    return length;
  }


  async calculateAngle(x,y){

    let y1=500-y+this.y0;
    let x1:number = 0;
    let a :number = 1;
    
    if(x<400){
      x1 = 400-x;
      a= -1;
    }
    else{
      x1 = x-400;
    }
    let theta = (Math.atan(x1/y1)* 180 / Math.PI)*a;
    console.log("angle : ",theta);
    theta= theta*120/72;
    console.log("calculated angle : ", theta);
    return theta ;
  }

  sleepDelay(milliseconds) {
    var start = new Date().getTime();
    for (var i = 0; i < 1e7; i++) {
        if ((new Date().getTime() - start) > milliseconds){
            break;
        }
    }
  }

  async isSolderingFunc(state:boolean){
    if(state){
      this.isSoldering = true;
    }
    else{this.isSoldering = false}
    return 'done'
  }

}
 
class Point {

  x:number;
  y:number;
}


class SoldPoint{
  length:number;
  angleStep:number;
}

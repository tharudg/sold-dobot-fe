import {BaseCaller} from './caller.basic';
import {Injectable} from '@angular/core';
import { CallerPath } from './caller.path';
@Injectable()
export class NodeCaller {
  constructor(
    public caller: BaseCaller,
    public path: CallerPath
  ) {

  }


  getImage() {
    return new Promise((resolve, reject) => {
      this.caller.post(this.path.image,{})
        .then(result => resolve(result))
        .catch(err => reject(err));
    });
  }

  setPoints(obj) {
    return new Promise((resolve, reject) => {
      this.caller.post(this.path.points,obj)
        .then(result => resolve(result))
        .catch(err => reject(err));
    });
  }

  DeleteById(id) {
    return new Promise((resolve, reject) => {
      this.caller.delete(this.path.delPoints(id),{})
        .then(result => resolve(result))
        .catch(err => reject(err));
    });
  }



}
